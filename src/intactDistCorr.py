"""
Discover whether correlation exists between the genetic interaction score and 
the distance in the genetic interaction network

Created: 28 March 2016
"""

import json
import math
import numpy as np
import os


def get_plot_data(giType):
    if giType == 'negative':
        netdist = json.load( open(os.path.join('..', 'data', 'negdist.json')) )
        flag = -1
        print('Using negative genetic...')
    else:
        netdist = json.load( open(os.path.join('..', 'data', 'posdist.json')) )
        flag = 1
        print('Using positive genetic...')
    rawSGApath = os.path.join('..', '..', 'DataDownload', 'Yeast_SGA', 
                    'sgadata_costanzo2009_rawdata_101120.txt')
    queryCol = 0  # using ORF
    arrayCol = 2  # using ORF
    scoreCol = 4
    pvalCol, pvalCut = 6, 0.05
    scores = list()
    dists = list()
    for line in open(rawSGApath):
        tokens = line.split('\t')
        score = float(tokens[scoreCol])
        pval = float(tokens[pvalCol])
        if flag*np.sign(score) == 1 and pval < pvalCut:
            if '_' in tokens[queryCol]:  # if query gene is TS or DAmP mutant
                queryGene = tokens[queryCol].split('_')[0]
            else:
                queryGene = tokens[queryCol]
            arrayGene = tokens[arrayCol]
            if queryGene in netdist and arrayGene in netdist:
                scores.append( math.fabs(score) )
                dists.append( netdist[queryGene][arrayGene] )
        else:
            pass
    return np.array(scores), np.array(dists) 

