#!/usr/bin/env python

"""
Processing of Cell Syst 2015 dataset. Details found in DataProcessing.ipynb. 

Created: 17 March 2016
"""

import csv
import itertools
import json
import os
import pubchempy as pcp


#------------------------------------------------------------------------------
# List all compounds from the 128x128 synergy screen (Table S7)
tableS7path = os.path.join('..', 'data', 'CellSyst', 'TableS7.csv')
tableS7 = open(tableS7path, newline='')
s7reader = csv.reader(tableS7)
for i in range(2):  # skip 1st 2 rows
    row = next(s7reader)
s7header = next(s7reader)
cmpd1col = s7header.index('Vendor id 1')
cmpd2col = s7header.index('Vendor id 2')
cmpds = set()  # Vendor IDs
for row in s7reader:
    cmpds.update([ row[cmpd1col], row[cmpd2col] ])
tableS7.close()
print('There are', len(cmpds), 'unique compounds from the synergy screen.')


#------------------------------------------------------------------------------
# Convert each vendor ID to Canonical SMILES (Table S2)
tableS2path = os.path.join('..', 'data', 'CellSyst', 'TableS2.txt')
tableS2 = open(tableS2path, encoding='utf_16_le')
for i in range(2):  # skip 1st 2 rows
    line = tableS2.readline()
s2header = tableS2.readline().strip().split('\t')
idCol = s2header.index('Chemgrid')
smilesCol = s2header.index('Canonical_smiles')
id2smiles = dict()
for line in tableS2:
    tokens = line.strip().split('\t')
    vendorID = tokens[idCol]
    if vendorID in cmpds:
        id2smiles[vendorID] = tokens[smilesCol]
tableS2.close()
# 6 compounds were not found, 4 of which are manually added back below
id2smiles['SPE00201081'] = 'COC1=CC(=C(C(=C1)OC)C(=O)C2=CC=CC=C2)O'
id2smiles['SPE00210025'] = 'CC(=O)OCC(CC(CCCCCCCCCCCC=C)O)O.CC(=O)OCC(CC(CCCC'\
        'CCCCCCCC#C)O)O'
id2smiles['SPE00210567'] = 'COC1=C(C=C(C=C1)C=CC(=O)O)OC'
id2smiles['SPE00310300'] = 'CC=C(C)C(=O)OC1CCC2(C3C1(OC24CC5(C6CN7CC(CCC7C(C6'\
        '(C(CC5(C4CC3)O)O)O)(C)O)C)O)O)C.OS(=O)(=O)O'


#------------------------------------------------------------------------------
# Load/create dictionary converting Canonical SMILES to CID (saved to JSON)
smiles2cidSave = os.path.join('..', 'data', 'CellSyst', 
        'synergy_screen_smiles2cid.json')
if os.path.isfile(smiles2cidSave):
    smiles2cid = json.load(open(smiles2cidSave))
else:
    smiles2cid = dict()
    for vendorID in id2smiles:
        smiles = id2smiles[vendorID]
        results = pcp.get_compounds(smiles, 'smiles')
        smiles2cid[smiles] = results[0].cid  # unique CID
    json.dump(smiles2cid, open(smiles2cidSave, 'w'))


#------------------------------------------------------------------------------
# Now find the inhibition targets from STITCH for each vendor ID
STITCHfile = os.path.join('..', '..', 'DataProcessed', 'STITCH_cid2inhibit.json')
cid2inhibit = json.load(open(STITCHfile))
vend2inhibit = dict()
for vendorID in id2smiles:
    cid = 'CID{:0>9d}'.format(smiles2cid[ id2smiles[vendorID] ])
    if cid in cid2inhibit:
        vend2inhibit[vendorID] = cid2inhibit[cid]
vend2inhibitSave = os.path.join('..', 'data', 'CellSyst', 'vendID2inhibit.json')
json.dump(vend2inhibit, open(vend2inhibitSave, 'w'))
print('There are', len(vend2inhibit), 'compounds that have inhibition targets '\
        'listed in STITCH.')


#------------------------------------------------------------------------------
# Of all possible pairs of compounds having inhibition targets from STITCH, how 
# many were screened? Of these, which are synergistic or antagonistic? Create a
# gold standard text file
goldstdfilepath = os.path.join('..', 'data', 'CellSyst', 'synergyGoldStd.txt')
goldstdfile = open(goldstdfilepath, 'w')
pairsWithTargets = set([ frozenset(t) for t in 
    itertools.combinations(vend2inhibit.keys(), 2) ])
tableS7 = open(tableS7path, newline='')  # assemble all screened pairs
s7reader = csv.reader(tableS7)
for i in range(3):  # skip 1st 3 rows, header already read above
    row = next(s7reader)
synergistic = list()
antagonistic = list()
blissCol = s7header.index('Bliss independence')
for row in s7reader:
    pair = frozenset({ row[cmpd1col], row[cmpd2col] })
    if pair in pairsWithTargets:
        blissVal = float(row[blissCol])
        if blissVal > 0.25:
            synergistic.append( frozenset({ row[cmpd1col], row[cmpd2col] }) )
            goldstdfile.write('{}\t{}\t{:d}\n'.format(
                row[cmpd1col], row[cmpd2col], 1))
        elif blissVal < -0.18:
            antagonistic.append( frozenset({ row[cmpd1col], row[cmpd2col] }) )
            goldstdfile.write('{}\t{}\t{:d}\n'.format(
                row[cmpd1col], row[cmpd2col], 0))
        else:
            goldstdfile.write('{}\t{}\t{:d}\n'.format(
                row[cmpd1col], row[cmpd2col], 0))
tableS7.close()
goldstdfile.close()
print('There are', len(synergistic), 'synergistic pairs and', len(antagonistic),
        'antagonistic pairs that have known inhibition targets.')
if len(synergistic) > 0:
    print('The synergistic pairs are:')
    for pair in synergistic:
        cmpd1, cmpd2 = tuple(pair)
        print('{} + {}'.format(cmpd1, cmpd2))

