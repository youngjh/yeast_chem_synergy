# yeast_chem_synergy

Predicting chemical synergy and mechanism through genetic interactions in *Saccharomyces cerevisiae*.

# License

See [LICENSE](https://bitbucket.org/youngjh/yeast_chem_synergy/src/master/LICENSE.md) 
